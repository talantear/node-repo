import { Model } from 'mongoose';
import { applyPatches } from 'immer';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Node, NodeDocument } from './schemas/node.schema';
import ListReqDto from './dto/list-req.dto';
import ListRespDto from './dto/list-resp.dto';
import CreateNodeDto from './dto/create-node.dto';
import UpdateNodeDto from './dto/update-node.dto';
import DeleteNodeDto from './dto/delete-node.dto';
import PatchNodeDto from './dto/patch-node.dto';

@Injectable()
export class NodeService {
  constructor(@InjectModel(Node.name) private nodeModel: Model<NodeDocument>) {}

  async create(createNodeDto: CreateNodeDto): Promise<Node> {
    const createdNode = new this.nodeModel(createNodeDto);
    return createdNode.save();
  }

  async update(updateNodeDto: UpdateNodeDto): Promise<Node> {
    const node = await this.nodeModel.findOne({ _id: updateNodeDto.id });

    const a = this.nodeModel
      .updateOne({ _id: updateNodeDto.id }, { data: updateNodeDto.data })
      .exec();

    return a.then(() => this.nodeModel.findOne({ _id: updateNodeDto.id }));
  }

  async delete(deleteNodeDto: DeleteNodeDto): Promise<boolean> {
    return this.nodeModel
      .deleteOne({ _id: deleteNodeDto.id })
      .exec()
      .then((res) => res.deletedCount > 0);
  }

  async patch(patchNodeDto: PatchNodeDto): Promise<Node> {
    const node = await this.nodeModel.findOne({ _id: patchNodeDto.id });
    applyPatches(node.data, patchNodeDto.patches);

    return this.nodeModel
      .updateOne({ _id: node._id }, node)
      .exec()
      .then(() => node);
  }

  async getByType(type: string, reqDto: ListReqDto): Promise<ListRespDto> {
    const filter = { data: { type, data: reqDto.where } };
    const count = await this.nodeModel.find(filter).count();
    let q = this.nodeModel.find(filter);

    if (reqDto.sort) {
      q = q.sort(reqDto.sort);
    }

    if (reqDto.nPerPage) {
      q.skip(reqDto.page > 0 ? (reqDto.page - 1) * reqDto.nPerPage : 0).limit(
        reqDto.nPerPage,
      );
    }

    return q.exec().then((items: Node[]) => new ListRespDto(items, count));
  }
}
