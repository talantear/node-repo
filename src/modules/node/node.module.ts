import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NodeService } from './node.service';
import { Node, NodeSchema } from './schemas/node.schema';
import { NodeController } from './node.controller';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Node.name, schema: NodeSchema }]),
  ],
  controllers: [NodeController],
  providers: [NodeService],
})
export class NodeModule {}
