import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Meta } from './meta.schema';

export type NodeDocument = Node & Document;

@Schema()
export class Node {
  @Prop({ type: Array })
  data: Meta[];
}

export const NodeSchema = SchemaFactory.createForClass(Node);
