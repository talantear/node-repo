import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type MetaDocument = Meta & Document;

@Schema()
export class Meta {
  @Prop({ required: true })
  type: string;

  @Prop({ type: Object })
  data: object;
}

export const MetaSchema = SchemaFactory.createForClass(Meta);
