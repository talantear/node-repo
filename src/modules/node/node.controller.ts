import {
  Body,
  Controller,
  Delete,
  HttpCode,
  Param,
  Patch,
  Post,
  Put,
} from '@nestjs/common';
import { NodeService } from './node.service';
import { Node } from './schemas/node.schema';
import CreateNodeDto from './dto/create-node.dto';
import ListRespDto from './dto/list-resp.dto';
import ListReqDto from './dto/list-req.dto';
import UpdateNodeDto from './dto/update-node.dto';
import DeleteNodeDto from './dto/delete-node.dto';
import PatchNodeDto from './dto/patch-node.dto';

@Controller('node')
export class NodeController {
  constructor(private readonly nodeService: NodeService) {}

  @Post()
  create(@Body() createNodeDto: CreateNodeDto): Promise<Node> {
    return this.nodeService.create(createNodeDto);
  }

  @Delete()
  delete(@Body() deleteNodeDto: DeleteNodeDto): Promise<boolean> {
    return this.nodeService.delete(deleteNodeDto);
  }

  @Put()
  update(@Body() updateNodeDto: UpdateNodeDto): Promise<Node> {
    return this.nodeService.update(updateNodeDto);
  }

  @Patch()
  patch(@Body() patchNodeDto: PatchNodeDto): Promise<Node> {
    return this.nodeService.patch(patchNodeDto);
  }

  @Post('get-by-type/:type')
  @HttpCode(200)
  getByType(
    @Param('type') type: string,
    @Body() reqDto: ListReqDto,
  ): Promise<ListRespDto> {
    return this.nodeService.getByType(type, reqDto);
  }
}
