class DeleteNodeDto {
  id: string;

  constructor(id: string) {
    this.id = id;
  }
}

export default DeleteNodeDto;
