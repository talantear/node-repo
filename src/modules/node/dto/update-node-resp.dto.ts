import { Patch } from 'immer';
import { Node } from '../schemas/node.schema';

class UpdateNodeRespDto {
  node: Node;
  patches: Patch[];

  constructor(node: Node, patches: Patch[]) {
    this.node = node;
    this.patches = patches;
  }
}

export default UpdateNodeRespDto;
