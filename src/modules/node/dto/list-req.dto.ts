class ListReqDto {
  where: object;
  sort?: object;
  page?: number;
  nPerPage?: number;

  constructor(where: object, sort: object, page: number, nPerPage: number) {
    this.where = where;
    this.sort = sort;
    this.page = page;
    this.nPerPage = nPerPage;
  }
}

export default ListReqDto;
