import { Patch } from 'immer';

class PatchNodeDto {
  id: string;
  patches: Patch[];

  constructor(id: string, patches: Patch[]) {
    this.id = id;
    this.patches = patches;
  }
}

export default PatchNodeDto;
