import { Meta } from '../schemas/meta.schema';

class CreateNodeDto {
  data: Meta[];

  constructor(data: Meta[]) {
    this.data = data;
  }
}

export default CreateNodeDto;
