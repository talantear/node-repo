import { Meta } from '../schemas/meta.schema';

class UpdateNodeDto {
  id: string;
  data: Meta[];

  constructor(id: string, data: Meta[]) {
    this.id = id;
    this.data = data;
  }
}

export default UpdateNodeDto;
