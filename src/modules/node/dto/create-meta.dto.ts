class CreateMetaDto {
  type: string;
  data: object;

  constructor(type: string, data: object) {
    this.type = type;
    this.data = data;
  }
}

export default CreateMetaDto;
