import { Node } from '../schemas/node.schema';

class ListRespDto {
  items: Node[];
  totalCount: number;

  constructor(items: Node[], totalCount: number) {
    this.items = items;
    this.totalCount = totalCount;
  }
}

export default ListRespDto;
